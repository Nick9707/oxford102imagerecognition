**Easy way to try**  
*I recomend it if your do not have installed python, anaconda, cuda and pyTorch on your computer*

1. Go to Google Colab with your google account
2. Upload notebook Image Classifier Project (1).ipynb to Google Colab
3. Upload flower_data folder to your Google Drive(*Be carfuul it is ~303mb of data*)
4. Adjust all url in project(if you need)
5. Run each sell starting from the top in Google Colab

**GNU GENERAL PUBLIC LICENSE**  
This license was chosen because  I do not want anyone to make code proprietary. It is ok, if they want to sell it, but it is going to be hard, because they also should have this code as open source. I would like to see how my project may be develop and want to have ability to see all code.
